default rel

global sepia_sse
global sepia_avx

%define c11 0.393
%define c12 0.769
%define c13 0.189
%define c21 0.349
%define c22 0.686
%define c23 0.168
%define c31 0.272
%define c32 0.534
%define c33 0.131

; matrix:
; 0.393, 0.769, 0.189,
; 0.349, 0.686, 0.168,
; 0.272, 0.534, 0.131
section .rodata
    red_coefs:      dd c11, c21, c31, c11, c21, c31, c11, c21, c31, c11
    green_coefs:    dd c12, c22, c32, c12, c22, c32, c12, c22, c32, c12
    blue_coefs:     dd c13, c23, c33, c13, c23, c33, c13, c23, c33, c13

section .text
; movdqa - move double qword aligned
; mulps - multiply packed single precision
; addps - packed singled precision
;
; formula:
; R = r*c_11 + g*c_12 + b*c_13
; G = r*c_21 + g*c_22 + b*c_23
; B = r*c_31 + g*c_32 + b*c_33
; 
; conversions:
; cvtdq2ps: x4 int32 -> 4x float
; cvtps2dq: 4x float -> int32
; cvtpi2ps: 2x int32 -> 2x float in mm/xmm (don't use)
;
; saturation packing:
; packusdw: 4x int32 -> 8x int16
; packuswb: 8x int16 -> 16x int8
; 
; unpacking:
; punpcklbw: --------abcdEFGH -> -a-b-c-d-E-F-G-H (unpack bytes)
; punpcklwd: --------abcdefgh -> --ab--cd--ef--gh (unpack words)
;
; pixel structure: r, g, b
; rdi = float src[12] ; r1 g1 b1 r2 g2 b2 r3 g3 b3 r4 g4 b4
; rsi = float dst[12]
sepia_sse:
    sub rsp, 16
    pxor  xmm6, xmm6 ; required for saturation

    mov eax, dword [rdi]
    mov dword [rsp], eax
    mov dword [rsp+4], eax
    mov dword [rsp+8], eax
    mov eax, dword [rdi+12]
    mov dword [rsp+12], eax
    movups xmm0, [rsp]  ; xmm0 = r1 | r1 | r1 | r2

    mov eax, dword [rdi+4]
    mov dword [rsp], eax
    mov dword [rsp+4], eax
    mov dword [rsp+8], eax
    mov eax, dword [rdi+16]
    mov dword [rsp+12], eax
    movups xmm1, [rsp]  ; xmm1 = g1 | g1 | g1 | g2

    mov eax, dword [rdi+8]
    mov dword [rsp], eax
    mov dword [rsp+4], eax
    mov dword [rsp+8], eax
    mov eax, dword [rdi+20]
    mov dword [rsp+12], eax
    movups xmm2, [rsp]  ; xmm2 = b1 | b1 | b1 | b2

    movups xmm3, [red_coefs]    ; xmm3 = c11 | c21 | c31 | c11
    movups xmm4, [green_coefs]  ; xmm4 = c12 | c22 | c32 | c12
    movups xmm5, [blue_coefs]   ; xmm5 = c13 | c23 | c33 | c13

    mulps xmm0, xmm3    ; xmm0 = r1*c11 | r1*c21 | r1*c31 | r2*c11
    mulps xmm1, xmm4    ; xmm1 = g1*c12 | g1*c22 | g1*c32 | g2*c12
    mulps xmm2, xmm5    ; xmm2 = b1*c13 | b1*c23 | b1*c33 | b2*c13

    addps xmm0, xmm1
    addps xmm0, xmm2    ; xmm0 = R1 | G1 | B1 | R2
    
    cvtps2dq xmm0, xmm0 ; float -> int32
    packusdw xmm0, xmm0 ; int32 -> int16
    packuswb xmm0, xmm0 ; int16 -> int8
    punpcklbw xmm0, xmm6 ; int8 -> int16
    punpcklwd xmm0, xmm6 ; int16 -> int32
    cvtdq2ps xmm0, xmm0 ; int32 -> float

    movups [rsi], xmm0  ; 1.33/4 pixels done

    mov eax, dword [rdi+12]
    mov dword [rsp], eax
    mov dword [rsp+4], eax
    mov eax, dword [rdi+24]
    mov dword [rsp+8], eax
    mov dword [rsp+12], eax
    movups xmm0, [rsp]  ; xmm0 = r2 | r2 | r3 | r3

    mov eax, dword [rdi+16]
    mov dword [rsp], eax
    mov dword [rsp+4], eax
    mov eax, dword [rdi+28]
    mov dword [rsp+8], eax
    mov dword [rsp+12], eax
    movups xmm1, [rsp]  ; xmm1 = g2 | g2 | g3 | g3

    mov eax, dword [rdi+20]
    mov dword [rsp], eax
    mov dword [rsp+4], eax
    mov eax, dword [rdi+32]
    mov dword [rsp+8], eax
    mov dword [rsp+12], eax
    movups xmm2, [rsp]  ; xmm2 = b2 | b2 | b3 | b3

    movups xmm3, [red_coefs + 4]    ; xmm3 = c21 | c31 | c11 | c21
    movups xmm4, [green_coefs + 4]  ; xmm4 = c22 | c32 | c12 | c22
    movups xmm5, [blue_coefs + 4]   ; xmm5 = c23 | c33 | c13 | c23

    mulps xmm0, xmm3    ; xmm0 = r2*c21 | r2*c31 | r3*c11 | r3*c21
    mulps xmm1, xmm4    ; xmm1 = g2*c22 | g2*c32 | g3*c12 | g3*c22
    mulps xmm2, xmm5    ; xmm2 = b2*c23 | b2*c33 | b3*c13 | b3*c23

    addps xmm0, xmm1
    addps xmm0, xmm2        ; xmm0 = R2 | G2 | B3 | R3

    cvtps2dq xmm0, xmm0 ; float -> int32
    packusdw xmm0, xmm0 ; int32 -> int16
    packuswb xmm0, xmm0 ; int16 -> int8
    punpcklbw xmm0, xmm6 ; int8 -> int16
    punpcklwd xmm0, xmm6 ; int16 -> int32
    cvtdq2ps xmm0, xmm0 ; int32 -> float

    movups [rsi+16], xmm0   ; 2.66/4 pixels done

    mov eax, dword [rdi+24]
    mov dword [rsp], eax
    mov eax, dword [rdi+36]
    mov dword [rsp+4], eax
    mov dword [rsp+8], eax
    mov dword [rsp+12], eax
    movups xmm0, [rsp]      ; xmm0 = r3 | r4 | r4 | r4

    mov eax, dword [rdi+28]
    mov dword [rsp], eax
    mov eax, dword [rdi+40]
    mov dword [rsp+4], eax
    mov dword [rsp+8], eax
    mov dword [rsp+12], eax
    movups xmm1, [rsp]      ; xmm1 = g3 | g4 | g4 | g4

    mov eax, dword [rdi+32]
    mov dword [rsp], eax
    mov eax, dword [rdi+44]
    mov dword [rsp+4], eax
    mov dword [rsp+8], eax
    mov dword [rsp+12], eax
    movups xmm2, [rsp]      ; xmm2 = b3 | b4 | b4 | b4

    movups xmm3, [red_coefs + 8]    ; xmm3 = c31 | c11 | c21 | c31
    movups xmm4, [green_coefs + 8]  ; xmm4 = c32 | c12 | c22 | c32
    movups xmm5, [blue_coefs + 8]   ; xmm5 = c33 | c13 | c23 | c33

    mulps xmm0, xmm3    ; xmm0 = r3*c31 | r4*c11 | r4*c21 | r4*c31
    mulps xmm1, xmm4    ; xmm1 = g3*c32 | g4*c12 | g4*c22 | g4*c32
    mulps xmm2, xmm5    ; xmm2 = b3*c33 | b4*c13 | b4*c23 | b4*c33

    addps xmm0, xmm1
    addps xmm0, xmm2        ; xmm0 = R3 | G4 | B4 | R4

    cvtps2dq xmm0, xmm0 ; float -> int32
    packusdw xmm0, xmm0 ; int32 -> int16
    packuswb xmm0, xmm0 ; int16 -> int8
    punpcklbw xmm0, xmm6 ; int8 -> int16
    punpcklwd xmm0, xmm6 ; int16 -> int32
    cvtdq2ps xmm0, xmm0 ; int32 -> float

    movups [rsi+32], xmm0   ; 4/4 pixels done
    add rsp, 16
    ret

sepia_avx:
    sub rsp, 32
    vpxor ymm6, ymm6, ymm6 ; required for saturation

    mov eax, dword [rdi]
    mov dword [rsp], eax
    mov dword [rsp+4], eax
    mov dword [rsp+8], eax
    mov eax, dword [rdi+12]
    mov dword [rsp+12], eax
    mov dword [rsp+16], eax
    mov dword [rsp+20], eax
    mov eax, dword [rdi+24]
    mov dword [rsp+24], eax
    mov dword [rsp+28], eax
    vmovups ymm0, [rsp]  ; ymm0 = r1 | r1 | r1 | r2 | r2 | r2 | r3 | r3
    
    mov eax, dword [rdi+4]
    mov dword [rsp], eax
    mov dword [rsp+4], eax
    mov dword [rsp+8], eax
    mov dword eax, dword [rdi+16]
    mov dword [rsp+12], eax
    mov dword [rsp+16], eax
    mov dword [rsp+20], eax
    mov dword eax, dword [rdi+28]
    mov dword [rsp+24], eax
    mov dword [rsp+28], eax
    vmovups ymm1, [rsp]  ; ymm1 = g1 | g1 | g1 | g2 | g2 | g2 | g3 | g3

    mov eax, dword [rdi+8]
    mov dword [rsp], eax
    mov dword [rsp+4], eax
    mov dword [rsp+8], eax
    mov eax, dword [rdi+20]
    mov dword [rsp+12], eax
    mov dword [rsp+16], eax
    mov dword [rsp+20], eax
    mov eax, dword [rdi+32]
    mov dword [rsp+24], eax
    mov dword [rsp+28], eax
    vmovups ymm1, [rsp]  ; ymm2 = b1 | b1 | b1 | b2 | b2 | b2 | b3 | b3
    
    vmulps ymm0, [red_coefs]    ; ymm0 = r1*c11 | r1*c21 | r1*c31 | r2*c11 | r2*c21 | r2*c31 | r3*c11 | r3*c21
    vmulps ymm1, [green_coefs]    ; ymm1 = g1*c12 | g1*c22 | g1*c32 | g2*c12 | g2*c22 | g2*c32 | g3*c12 | g3*c22
    vmulps ymm2, [blue_coefs]   ; ymm2 = b1*c13 | b1*c23 | b1*c33 | b2*c13 | b2*c23 | b2*c33 | b3*c13 | b3*c23

    vaddps ymm0, ymm1
    vaddps ymm0, ymm2    ; ymm0 = R1 | G1 | B1 | R2 | G2 | B2 | R3 | G3

    vcvtps2dq ymm0, ymm0 ; float -> int32
    vpackusdw ymm0, ymm0 ; 8x int32 -> 16x int16
    vpackuswb ymm0, ymm0 ; 16x int16 -> 32x int8
    vpunpcklbw ymm0, ymm6 ; 32x int8 -> 16x int16
    vpunpcklwd ymm0, ymm6 ; 16x int16 -> 8x int32
    vcvtdq2ps ymm0, ymm0 ; int32 -> float

    vmovups [rsi], ymm0  ; 2.66/8 pixels done

    mov eax, dword [rdi+24]
    mov dword [rsp], eax
    mov eax, dword [rdi+36]
    mov dword [rsp+4], eax
    mov dword [rsp+8], eax
    mov dword [rsp+12], eax
    mov eax, dword [rdi+48]
    mov dword [rsp+16], eax
    mov dword [rsp+20], eax
    mov dword [rsp+24], eax
    mov eax, dword [rdi+60]
    mov dword [rsp+28], eax
    vmovups ymm0, [rsp]  ; ymm0 = r3 | r4 | r4 | r4 | r5 | r5 | r5 | r6
    
    mov eax, dword [rdi+28]
    mov dword [rsp], eax
    mov eax, dword [rdi+40]
    mov dword [rsp+4], eax
    mov dword [rsp+8], eax
    mov dword [rsp+12], eax
    mov eax, dword [rdi+52]
    mov dword [rsp+16], eax
    mov dword [rsp+20], eax
    mov dword [rsp+24], eax
    mov eax, dword [rdi+64]
    mov dword [rsp+28], eax
    vmovups ymm1, [rsp]  ; ymm1 = g3 | g4 | g4 | g4 | g5 | g5 | g5 | g6

    mov eax, dword [rdi+32]
    mov dword [rsp], eax
    mov eax, dword [rdi+44]
    mov dword [rsp+4], eax
    mov dword [rsp+8], eax
    mov dword [rsp+12], eax
    mov eax, dword [rdi+56]
    mov dword [rsp+16], eax
    mov dword [rsp+20], eax
    mov dword [rsp+24], eax
    mov eax, dword [rdi+68]
    mov dword [rsp+28], eax
    vmovups ymm1, [rsp]  ; ymm2 = b3 | b4 | b4 | b4 | b5 | b5 | b5 | b6

    vmulps ymm0, [red_coefs+8]
    vmulps ymm1, [green_coefs+8]
    vmulps ymm2, [blue_coefs+8]

    vaddps ymm0, ymm1
    vaddps ymm0, ymm2

    vcvtps2dq ymm0, ymm0 ; float -> int32
    vpackusdw ymm0, ymm0 ; int32 -> int16
    vpackuswb ymm0, ymm0 ; int16 -> int8
    vpunpcklbw ymm0, ymm6 ; int8 -> int16
    vpunpcklwd ymm0, ymm6 ; int16 -> int32
    vcvtdq2ps ymm0, ymm0 ; int32 -> float

    vmovups [rsi+32], ymm0  ; 5.32/8 pixels done

    mov eax, dword [rdi+60]
    mov dword [rsp], eax
    mov dword [rsp+4], eax
    mov eax, dword [rdi+72]
    mov dword [rsp+8], eax
    mov dword [rsp+12], eax
    mov dword [rsp+16], eax
    mov eax, dword [rdi+84]
    mov dword [rsp+20], eax
    mov dword [rsp+24], eax
    mov dword [rsp+28], eax
    vmovups ymm0, [rsp]  ; ymm0 = r6 | r6 | r7 | r7 | r7 | r8 | r8 | r8
    
    mov eax, dword [rdi+64]
    mov dword [rsp], eax
    mov dword [rsp+4], eax
    mov eax, dword [rdi+76]
    mov dword [rsp+8], eax
    mov dword [rsp+12], eax
    mov dword [rsp+16], eax
    mov eax, dword [rdi+88]
    mov dword [rsp+20], eax
    mov dword [rsp+24], eax
    mov dword [rsp+28], eax
    vmovups ymm1, [rsp]  ; ymm1 = g6 | g6 | g7 | g7 | g7 | g8 | g8 | g8

    mov eax, dword [rdi+68]
    mov dword [rsp], eax
    mov dword [rsp+4], eax
    mov eax, dword [rdi+80]
    mov dword [rsp+8], eax
    mov dword [rsp+12], eax
    mov dword [rsp+16], eax
    mov eax, dword [rdi+92]
    mov dword [rsp+20], eax
    mov dword [rsp+24], eax
    mov dword [rsp+28], eax
    vmovups ymm1, [rsp]  ; ymm2 = b6 | b6 | b7 | b7 | b7 | b8 | b8 | b8

    vmulps ymm0, [red_coefs+4]
    vmulps ymm1, [green_coefs+4]
    vmulps ymm2, [blue_coefs+4]

    vaddps ymm0, ymm1
    vaddps ymm0, ymm2

    vcvtps2dq ymm0, ymm0 ; float -> int32
    vpackusdw ymm0, ymm0 ; int32 -> int16
    vpackuswb ymm0, ymm0 ; int16 -> int8
    vpunpcklbw ymm0, ymm6 ; int8 -> int16
    vpunpcklwd ymm0, ymm6 ; int16 -> int32
    vcvtdq2ps ymm0, ymm0 ; int32 -> float

    vmovups [rsi+64], ymm0  ; 8/8 pixels done

    add rsp, 32
    ret