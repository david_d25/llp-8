#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "image.h"

const pixel IMAGE_DEFAULT_PIXEL = {0, 0, 0, 0};

struct image {
    uint32_t width, height;
    pixel* data;
};

image* image_copy(image* src) {
    if (src == NULL)
        return NULL;

    uint32_t img_width = image_get_width(src);
    uint32_t img_height = image_get_height(src);

    image* new_image = image_create(img_width, img_height);
    memcpy(new_image->data, src->data, sizeof(pixel)*img_width*img_height);
    return new_image;
}

image* image_create(uint32_t width, uint32_t height) {
    struct image* result = (image*)malloc(sizeof(image));
    result->width = width;
    result->height = height;
    result->data = (pixel*)malloc(sizeof(pixel) * width * height);
    return result;
}

void image_destroy(image* img) {
    if (img != NULL && img->data != NULL)
        free(img->data);
        
    if (img != NULL)
        free(img);
}

uint32_t image_get_width(image* img) {
    if (img == NULL)
        return 0;
    return img->width;
}

uint32_t image_get_height(image* img) {
    if (img == NULL)
        return 0;
    return img->height;
}

bool image_is_point_in_bounds(image* img, point p) {
    if (img == NULL)
        return false;

    return !(
        p.x < 0 ||
        p.x >= image_get_width(img) ||
        p.y < 0 ||
        p.y >= image_get_height(img)
    );
}

pixel image_get_pixel_fast(image* img, point p) {
    return img->data[image_get_width(img)*p.y + p.x];
}

pixel image_get_pixel_or_default(image* img, point p, pixel default_pixel) {
    if (!image_is_point_in_bounds(img, p))
        return default_pixel;
    return image_get_pixel_fast(img, p);
}

pixel image_get_pixel(image* img, point p) {
    return image_get_pixel_or_default(img, p, IMAGE_DEFAULT_PIXEL);
}

pixel image_get_pixel_raw(image* img, size_t index) {
    if (img == NULL || index >= img->width*img->height)
        return IMAGE_DEFAULT_PIXEL;
    return img->data[index];
}

void image_set_pixel_raw(image* img, size_t index, pixel pixel) {
    if (img == NULL || index >= img->width*img->height)
        return;
    img->data[index] = pixel;
}

void image_set_pixel(image* img, point p, pixel pixel) {
    if (!image_is_point_in_bounds(img, p))
        return;
    img->data[image_get_width(img)*p.y + p.x] = pixel;
}

void image_swap_size(image* img) {
    if (img == NULL)
        return;

    uint32_t buffer = img->width;
    img->width = img->height;
    img->height = buffer;
}