#ifndef _IMAGE_IO_H
#define _IMAGE_IO_H

#include <stdio.h>

#include "image.h"

typedef enum image_load_status {
    IMAGE_LOAD_OK = 0,
    IMAGE_LOAD_FILE_NOT_EXIST,
    IMAGE_LOAD_READ_FAIL,
    IMAGE_LOAD_TYPE_NOT_SUPPORTED,
    IMAGE_LOAD_BPP_NOT_SUPPORTED,
    IMAGE_LOAD_COMPRESSION_NOT_SUPPORTED
} image_load_status;


typedef enum image_save_status {
    IMAGE_SAVE_OK = 0,
    IMAGE_SAVE_NO_ACCESS,
    IMAGE_SAVE_OPEN_FAIL,
    IMAGE_SAVE_WRITE_FAIL
} image_save_status;

image_load_status image_load_bmp(image**, char*);
image_save_status image_save_bmp(image*, char*);

#endif