#ifndef _IMAGE_UTILS_H
#define _IMAGE_UTILS_H

#include <stdint.h>

#include "image.h"

void image_blur(image*, int);
void image_rotate(image*, double);
void image_mirror(image*, bool, bool);
void image_sepia_std(image*);
void image_sepia_sse(image*);
void image_sepia_avx(image*);

#endif