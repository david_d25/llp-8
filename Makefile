BUILD_DIR = build
CFLAGS = -pedantic-errors -Wall -Werror

all: build_dir main.o
	gcc $(CFLAGS) -o $(BUILD_DIR)/lab8 $(BUILD_DIR)/*.o -lm

main.o: build_dir main.c operations.o image_io.o
	gcc $(CFLAGS) -c -o $(BUILD_DIR)/main.o main.c

operations.o: build_dir image_utils.o operations.c
	gcc $(CFLAGS) -c -o $(BUILD_DIR)/operations.o operations.c

image_utils.o: build_dir image.o sepia.o image_utils.c
	gcc $(CFLAGS) -c -o $(BUILD_DIR)/image_utils.o image_utils.c

image_io.o: build_dir image.o image_io.c
	gcc $(CFLAGS) -c -o $(BUILD_DIR)/image_io.o image_io.c

image.o: build_dir image.c
	gcc $(CFLAGS) -c -o $(BUILD_DIR)/image.o image.c

sepia.o: build_dir sepia.asm
	nasm -felf64 -o $(BUILD_DIR)/sepia.o sepia.asm

build_dir:
	mkdir -p build

clean:
	rm $(BUILD_DIR)/*.o lab6 2> /dev/null || true