#ifndef _SEPIA_H
#define _SEPIA_H

extern void sepia_sse(float src[12], float dst[12]);
extern void sepia_avx(float src[24], float dst[24]);

#endif